import base64
import time
import json
import requests
from datetime import datetime

print("Getting ready...")
ipEsp32Cam = 'http://192.168.1.131'
retryCount = 0

print("\nPreparing request...")
reqUrl = "https://pks-api.mioto.io/api"

print("Getting token...")
tokenPage = requests.get('http://pks-api.mioto.io/login')
reqToken = json.loads(tokenPage.content.decode())['token']

reqHeaders = {
	"Content-Type" : "application/json",
	"Auth-Token" : reqToken
}

while(True):
	try:
		print("\rGetting image from camera")
		reqCaptureCam = requests.get('%s/capture' % (ipEsp32Cam))
		byteCapture = reqCaptureCam.content[reqCaptureCam.content.find(bytes('\xff\xd8', 'utf-8')):reqCaptureCam.content.find(bytes('\xff\xd9', 'utf-8'))+2]
		jpgCctvCapture64 = base64.b64encode(byteCapture)

		reqBody = {
			"Request" : "api_logcctv_insert_update",
			"p_intkendaraanid": 3,
			"p_intcctvid" : 1,
			"p_dtwaktu" : datetime.today().strftime('%Y-%m-%d %H:%M:%S.%f'),
			"p_txtfoto" : jpgCctvCapture64.decode()
		}

		print("Sending request...")
		reqPost = requests.post(reqUrl, json.dumps(reqBody), headers=reqHeaders)
		reqStatus = reqPost.status_code

		print("Response: %s" % (reqPost.content))

		if reqStatus != 200:
			raise ConnectionError("Request is not succeed (%s)." % (reqStatus))

		time.sleep(10)

	except Exception as e:
		retryCount += 1
		print("Error:", e, ". Retry count:", retryCount)
		continue

	except KeyboardInterrupt:
		print("Aborted.")
		break
		
print("Exit")

# videoCapture.release()